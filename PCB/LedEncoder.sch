EESchema Schematic File Version 4
LIBS:LedEncoder-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Amplifier_Operational:MCP6002-xMS U2
U 1 1 5C5C85E9
P 2850 3300
F 0 "U2" H 2850 2933 50  0000 C CNN
F 1 "MCP6002-xMS" H 2850 3024 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 2850 3300 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21733j.pdf" H 2850 3300 50  0001 C CNN
	1    2850 3300
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:MCP6002-xMS U2
U 2 1 5C5C8665
P 5150 3200
F 0 "U2" H 5200 3350 50  0000 C CNN
F 1 "MCP6002-xMS" H 5200 3450 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 5150 3200 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21733j.pdf" H 5150 3200 50  0001 C CNN
	2    5150 3200
	1    0    0    1   
$EndComp
$Comp
L Amplifier_Operational:MCP6002-xMS U2
U 3 1 5C5C86A2
P 4550 1250
F 0 "U2" H 4508 1296 50  0000 L CNN
F 1 "MCP6002-xMS" H 4508 1205 50  0000 L CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 4550 1250 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21733j.pdf" H 4550 1250 50  0001 C CNN
	3    4550 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5C5C891F
P 2800 2750
F 0 "R3" V 2593 2750 50  0000 C CNN
F 1 "1M" V 2684 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2730 2750 50  0001 C CNN
F 3 "~" H 2800 2750 50  0001 C CNN
	1    2800 2750
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5C5C8974
P 2800 2400
F 0 "C2" V 2548 2400 50  0000 C CNN
F 1 "10p" V 2639 2400 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2838 2250 50  0001 C CNN
F 3 "~" H 2800 2400 50  0001 C CNN
	1    2800 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	2950 2750 3350 2750
Wire Wire Line
	3350 2750 3350 3300
Wire Wire Line
	3350 3300 3200 3300
Wire Wire Line
	2950 2400 3350 2400
Wire Wire Line
	3350 2400 3350 2750
Connection ~ 3350 2750
Wire Wire Line
	2650 2750 2450 2750
Wire Wire Line
	2450 3200 2550 3200
Wire Wire Line
	2450 2750 2450 3200
Wire Wire Line
	2450 2400 2450 2750
Connection ~ 2450 2750
$Comp
L Device:R R2
U 1 1 5C5C8BA2
P 2050 3650
F 0 "R2" H 2120 3696 50  0000 L CNN
F 1 "4.7k" H 2120 3605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1980 3650 50  0001 C CNN
F 3 "~" H 2050 3650 50  0001 C CNN
	1    2050 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5C5C8BE8
P 1800 3400
F 0 "R1" V 1600 3450 50  0000 C CNN
F 1 "10k" V 1700 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 1730 3400 50  0001 C CNN
F 3 "~" H 1800 3400 50  0001 C CNN
	1    1800 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1950 3400 2050 3400
Wire Wire Line
	2050 3400 2050 3500
Connection ~ 2050 3400
Wire Wire Line
	2050 3800 2050 3850
$Comp
L power:GND #PWR07
U 1 1 5C5C8F03
P 2050 3850
F 0 "#PWR07" H 2050 3600 50  0001 C CNN
F 1 "GND" H 2055 3677 50  0000 C CNN
F 2 "" H 2050 3850 50  0001 C CNN
F 3 "" H 2050 3850 50  0001 C CNN
	1    2050 3850
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 5C5C8F53
P 1500 3400
F 0 "#PWR03" H 1500 3250 50  0001 C CNN
F 1 "+3V3" V 1515 3528 50  0000 L CNN
F 2 "" H 1500 3400 50  0001 C CNN
F 3 "" H 1500 3400 50  0001 C CNN
	1    1500 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2450 2400 2650 2400
$Comp
L Device:C C5
U 1 1 5C5C97B3
P 3600 3300
F 0 "C5" V 3348 3300 50  0000 C CNN
F 1 "100n" V 3439 3300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3638 3150 50  0001 C CNN
F 3 "~" H 3600 3300 50  0001 C CNN
	1    3600 3300
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 5C5C9822
P 4000 3300
F 0 "C6" V 3748 3300 50  0000 C CNN
F 1 "100n" V 3839 3300 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4038 3150 50  0001 C CNN
F 3 "~" H 4000 3300 50  0001 C CNN
	1    4000 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5C5C9858
P 4300 2650
F 0 "R4" H 4370 2696 50  0000 L CNN
F 1 "56k" H 4370 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4230 2650 50  0001 C CNN
F 3 "~" H 4300 2650 50  0001 C CNN
	1    4300 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5C5C98BE
P 4300 3550
F 0 "R5" H 4370 3596 50  0000 L CNN
F 1 "56k" H 4370 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4230 3550 50  0001 C CNN
F 3 "~" H 4300 3550 50  0001 C CNN
	1    4300 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5C5C9A6C
P 4600 2650
F 0 "R6" H 4670 2696 50  0000 L CNN
F 1 "10k" H 4670 2605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4530 2650 50  0001 C CNN
F 3 "~" H 4600 2650 50  0001 C CNN
	1    4600 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5C5C9AB8
P 4600 3550
F 0 "R7" H 4670 3596 50  0000 L CNN
F 1 "10k" H 4670 3505 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 4530 3550 50  0001 C CNN
F 3 "~" H 4600 3550 50  0001 C CNN
	1    4600 3550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5C5C9B45
P 5250 2850
F 0 "R9" V 5043 2850 50  0000 C CNN
F 1 "47k" V 5134 2850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5180 2850 50  0001 C CNN
F 3 "~" H 5250 2850 50  0001 C CNN
	1    5250 2850
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5C5C9BA8
P 5250 2050
F 0 "R8" V 5450 2050 50  0000 C CNN
F 1 "220k" V 5350 2050 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5180 2050 50  0001 C CNN
F 3 "~" H 5250 2050 50  0001 C CNN
	1    5250 2050
	0    1    1    0   
$EndComp
Wire Wire Line
	5450 3200 5600 3200
Wire Wire Line
	5400 2850 5600 2850
Wire Wire Line
	5600 2850 5600 3000
Wire Wire Line
	5400 2050 5600 2050
Wire Wire Line
	5100 2850 4600 2850
Wire Wire Line
	4600 2850 4600 2800
Wire Wire Line
	4600 2850 4600 3100
Connection ~ 4600 2850
Wire Wire Line
	4850 3100 4600 3100
Connection ~ 4600 3100
Wire Wire Line
	4600 3100 4600 3400
Wire Wire Line
	4850 3300 4300 3300
Wire Wire Line
	4300 3300 4300 2800
Wire Wire Line
	4300 3300 4300 3400
Connection ~ 4300 3300
Wire Wire Line
	4600 2500 4600 2400
Wire Wire Line
	4600 2400 4300 2400
Wire Wire Line
	4300 2400 4300 2500
$Comp
L power:+3V3 #PWR012
U 1 1 5C5CB74E
P 4300 2350
F 0 "#PWR012" H 4300 2200 50  0001 C CNN
F 1 "+3V3" H 4315 2523 50  0000 C CNN
F 2 "" H 4300 2350 50  0001 C CNN
F 3 "" H 4300 2350 50  0001 C CNN
	1    4300 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2350 4300 2400
Connection ~ 4300 2400
Wire Wire Line
	5600 2050 5600 2850
Connection ~ 5600 2850
Wire Wire Line
	3800 2050 3800 3300
Wire Wire Line
	3800 3300 3850 3300
Wire Wire Line
	3800 3300 3750 3300
Connection ~ 3800 3300
Wire Wire Line
	3450 3300 3350 3300
Connection ~ 3350 3300
Wire Wire Line
	4150 3300 4300 3300
Wire Wire Line
	4300 3700 4300 3800
Wire Wire Line
	4300 3800 4600 3800
Wire Wire Line
	4600 3800 4600 3700
$Comp
L power:GND #PWR013
U 1 1 5C5CDF6E
P 4300 3850
F 0 "#PWR013" H 4300 3600 50  0001 C CNN
F 1 "GND" H 4305 3677 50  0000 C CNN
F 2 "" H 4300 3850 50  0001 C CNN
F 3 "" H 4300 3850 50  0001 C CNN
	1    4300 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 3800 4300 3850
Connection ~ 4300 3800
Wire Wire Line
	3800 2050 5100 2050
Connection ~ 2450 3200
$Comp
L power:GND #PWR010
U 1 1 5C5CF806
P 4450 1550
F 0 "#PWR010" H 4450 1300 50  0001 C CNN
F 1 "GND" H 4455 1377 50  0000 C CNN
F 2 "" H 4450 1550 50  0001 C CNN
F 3 "" H 4450 1550 50  0001 C CNN
	1    4450 1550
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR09
U 1 1 5C5CF833
P 4450 950
F 0 "#PWR09" H 4450 800 50  0001 C CNN
F 1 "+3V3" H 4465 1123 50  0000 C CNN
F 2 "" H 4450 950 50  0001 C CNN
F 3 "" H 4450 950 50  0001 C CNN
	1    4450 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5C5CF885
P 4000 1250
F 0 "C3" H 4115 1296 50  0000 L CNN
F 1 "100n" H 4115 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4038 1100 50  0001 C CNN
F 3 "~" H 4000 1250 50  0001 C CNN
	1    4000 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 950  4000 1100
Wire Wire Line
	4000 1400 4000 1550
Wire Wire Line
	4000 950  4450 950 
Connection ~ 4450 950 
Wire Wire Line
	4000 1550 4450 1550
Connection ~ 4450 1550
$Comp
L Device:D_Photo D1
U 1 1 5C5D280A
P 700 3550
F 0 "D1" V 604 3707 50  0000 L CNN
F 1 "D_Photo" V 695 3707 50  0000 L CNN
F 2 "OptoDevice:Osram_SMD-DIL2_4.5x4.0mm" H 650 3550 50  0001 C CNN
F 3 "~" H 650 3550 50  0001 C CNN
	1    700  3550
	0    1    1    0   
$EndComp
Wire Wire Line
	700  3200 700  3350
Wire Wire Line
	700  3650 700  3750
$Comp
L power:GND #PWR01
U 1 1 5C5D3CE7
P 700 3750
F 0 "#PWR01" H 700 3500 50  0001 C CNN
F 1 "GND" H 705 3577 50  0000 C CNN
F 2 "" H 700 3750 50  0001 C CNN
F 3 "" H 700 3750 50  0001 C CNN
	1    700  3750
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:MIC5504-3.3YM5 U1
U 1 1 5C5EEDEB
P 2300 1100
F 0 "U1" H 2300 1467 50  0000 C CNN
F 1 "MIC5504-3.3YM5" H 2300 1376 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 2300 700 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/MIC550X.pdf" H 2050 1350 50  0001 C CNN
	1    2300 1100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5C5EEF5B
P 1400 1250
F 0 "C1" H 1515 1296 50  0000 L CNN
F 1 "1u" H 1515 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1438 1100 50  0001 C CNN
F 3 "~" H 1400 1250 50  0001 C CNN
	1    1400 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5C5EF015
P 3000 1250
F 0 "C4" H 3115 1296 50  0000 L CNN
F 1 "1u" H 3115 1205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3038 1100 50  0001 C CNN
F 3 "~" H 3000 1250 50  0001 C CNN
	1    3000 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1000 3000 1000
Wire Wire Line
	3000 1000 3000 1100
Wire Wire Line
	3000 1400 3000 1500
$Comp
L power:GND #PWR08
U 1 1 5C5F0760
P 2300 1550
F 0 "#PWR08" H 2300 1300 50  0001 C CNN
F 1 "GND" H 2305 1377 50  0000 C CNN
F 2 "" H 2300 1550 50  0001 C CNN
F 3 "" H 2300 1550 50  0001 C CNN
	1    2300 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1500 2300 1500
Wire Wire Line
	2300 1500 2300 1400
Wire Wire Line
	2300 1500 2300 1550
Connection ~ 2300 1500
Wire Wire Line
	2300 1500 1400 1500
Wire Wire Line
	1400 1500 1400 1400
Wire Wire Line
	1400 1100 1400 1000
Wire Wire Line
	1400 1000 1800 1000
$Comp
L power:+3V3 #PWR011
U 1 1 5C5F3FBF
P 3300 900
F 0 "#PWR011" H 3300 750 50  0001 C CNN
F 1 "+3V3" H 3315 1073 50  0000 C CNN
F 2 "" H 3300 900 50  0001 C CNN
F 3 "" H 3300 900 50  0001 C CNN
	1    3300 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 1000 3300 1000
Wire Wire Line
	3300 1000 3300 900 
Connection ~ 3000 1000
$Comp
L Device:R R13
U 1 1 5C5F58AB
P 7250 2350
F 0 "R13" V 7043 2350 50  0000 C CNN
F 1 "15k" V 7134 2350 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 7180 2350 50  0001 C CNN
F 3 "~" H 7250 2350 50  0001 C CNN
	1    7250 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 5C5F59FD
P 6250 2650
F 0 "R11" V 6050 2650 50  0000 C CNN
F 1 "10k" V 6150 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6180 2650 50  0001 C CNN
F 3 "~" H 6250 2650 50  0001 C CNN
	1    6250 2650
	-1   0    0    1   
$EndComp
$Comp
L Device:R R12
U 1 1 5C5F5AC8
P 6250 3400
F 0 "R12" H 6180 3354 50  0000 R CNN
F 1 "10k" H 6180 3445 50  0000 R CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6180 3400 50  0001 C CNN
F 3 "~" H 6250 3400 50  0001 C CNN
	1    6250 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 2350 7550 2350
Wire Wire Line
	7550 3100 7400 3100
$Comp
L power:GND #PWR017
U 1 1 5C5F9302
P 7000 3400
F 0 "#PWR017" H 7000 3150 50  0001 C CNN
F 1 "GND" H 7005 3227 50  0000 C CNN
F 2 "" H 7000 3400 50  0001 C CNN
F 3 "" H 7000 3400 50  0001 C CNN
	1    7000 3400
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR016
U 1 1 5C5F9343
P 7000 2800
F 0 "#PWR016" H 7000 2650 50  0001 C CNN
F 1 "+3V3" H 7015 2973 50  0000 C CNN
F 2 "" H 7000 2800 50  0001 C CNN
F 3 "" H 7000 2800 50  0001 C CNN
	1    7000 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5C5F9384
P 7150 2800
F 0 "C7" V 6900 2850 50  0000 C CNN
F 1 "100n" V 7000 2900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 7188 2650 50  0001 C CNN
F 3 "~" H 7150 2800 50  0001 C CNN
	1    7150 2800
	0    1    1    0   
$EndComp
Wire Wire Line
	7300 2800 7350 2800
Wire Wire Line
	7350 2800 7350 2850
$Comp
L power:GND #PWR018
U 1 1 5C5FA85C
P 7350 2850
F 0 "#PWR018" H 7350 2600 50  0001 C CNN
F 1 "GND" H 7355 2677 50  0000 C CNN
F 2 "" H 7350 2850 50  0001 C CNN
F 3 "" H 7350 2850 50  0001 C CNN
	1    7350 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 2350 7550 3100
$Comp
L power:GND #PWR015
U 1 1 5C5FE7B3
P 6250 3600
F 0 "#PWR015" H 6250 3350 50  0001 C CNN
F 1 "GND" H 6255 3427 50  0000 C CNN
F 2 "" H 6250 3600 50  0001 C CNN
F 3 "" H 6250 3600 50  0001 C CNN
	1    6250 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 3600 6250 3550
$Comp
L power:+3V3 #PWR014
U 1 1 5C605293
P 6250 2500
F 0 "#PWR014" H 6250 2350 50  0001 C CNN
F 1 "+3V3" V 6265 2628 50  0000 L CNN
F 2 "" H 6250 2500 50  0001 C CNN
F 3 "" H 6250 2500 50  0001 C CNN
	1    6250 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR04
U 1 1 5C608551
P 1400 900
F 0 "#PWR04" H 1400 750 50  0001 C CNN
F 1 "+5V" H 1415 1073 50  0000 C CNN
F 2 "" H 1400 900 50  0001 C CNN
F 3 "" H 1400 900 50  0001 C CNN
	1    1400 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1400 900  1400 1000
Connection ~ 1400 1000
$Comp
L power:+5V #PWR02
U 1 1 5C609CEB
P 7150 1000
F 0 "#PWR02" H 7150 850 50  0001 C CNN
F 1 "+5V" H 7165 1173 50  0000 C CNN
F 2 "" H 7150 1000 50  0001 C CNN
F 3 "" H 7150 1000 50  0001 C CNN
	1    7150 1000
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR05
U 1 1 5C609D30
P 7350 1000
F 0 "#PWR05" H 7350 850 50  0001 C CNN
F 1 "+3V3" H 7365 1173 50  0000 C CNN
F 2 "" H 7350 1000 50  0001 C CNN
F 3 "" H 7350 1000 50  0001 C CNN
	1    7350 1000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5C609D75
P 7350 1350
F 0 "#PWR06" H 7350 1100 50  0001 C CNN
F 1 "GND" H 7355 1177 50  0000 C CNN
F 2 "" H 7350 1350 50  0001 C CNN
F 3 "" H 7350 1350 50  0001 C CNN
	1    7350 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1050 7150 1050
Wire Wire Line
	7150 1050 7150 1000
Wire Wire Line
	6900 1150 7350 1150
Wire Wire Line
	7350 1150 7350 1000
Wire Wire Line
	6900 1250 7350 1250
Wire Wire Line
	7350 1250 7350 1350
Text Label 5800 3000 0    50   ~ 0
Vout
Wire Wire Line
	7550 3100 7850 3100
Connection ~ 7550 3100
Text Label 7650 3100 0    50   ~ 0
Vout2
$Comp
L Connector:Conn_01x05_Male J1
U 1 1 5C610F88
P 6700 1250
F 0 "J1" H 6806 1628 50  0000 C CNN
F 1 "Conn_01x05_Male" H 6806 1537 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6700 1250 50  0001 C CNN
F 3 "~" H 6700 1250 50  0001 C CNN
	1    6700 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1350 7200 1350
Wire Wire Line
	6900 1450 7200 1450
Text Label 7000 1350 0    50   ~ 0
Vout
Text Label 7000 1450 0    50   ~ 0
Vout2
Wire Wire Line
	700  3200 2450 3200
$Comp
L Device:C C8
U 1 1 5C630458
P 2400 3650
F 0 "C8" H 2515 3696 50  0000 L CNN
F 1 "100n" H 2515 3605 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2438 3500 50  0001 C CNN
F 3 "~" H 2400 3650 50  0001 C CNN
	1    2400 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 3400 2400 3400
Wire Wire Line
	2400 3500 2400 3400
Connection ~ 2400 3400
Wire Wire Line
	2400 3400 2550 3400
$Comp
L power:GND #PWR019
U 1 1 5C6361E8
P 2400 3850
F 0 "#PWR019" H 2400 3600 50  0001 C CNN
F 1 "GND" H 2405 3677 50  0000 C CNN
F 2 "" H 2400 3850 50  0001 C CNN
F 3 "" H 2400 3850 50  0001 C CNN
	1    2400 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 3850 2400 3800
Wire Wire Line
	1500 3400 1650 3400
$Comp
L Device:C C9
U 1 1 5C63EEF3
P 6650 3400
F 0 "C9" H 6500 3400 50  0000 C CNN
F 1 "1u" H 6500 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6688 3250 50  0001 C CNN
F 3 "~" H 6650 3400 50  0001 C CNN
	1    6650 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	6650 3550 6650 3600
Wire Wire Line
	6650 3600 6250 3600
Connection ~ 6250 3600
Wire Wire Line
	6650 3250 6650 3200
Wire Wire Line
	6650 3200 6250 3200
Connection ~ 6250 3200
Wire Wire Line
	6250 3200 6250 3250
$Comp
L Device:R R14
U 1 1 5C6451D4
P 6550 3000
F 0 "R14" V 6343 3000 50  0000 C CNN
F 1 "1.5k" V 6434 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 6480 3000 50  0001 C CNN
F 3 "~" H 6550 3000 50  0001 C CNN
	1    6550 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	7100 2350 6750 2350
Wire Wire Line
	6750 2350 6750 3000
Wire Wire Line
	6750 3000 6800 3000
Wire Wire Line
	6750 3000 6700 3000
Connection ~ 6750 3000
$Comp
L Device:R R10
U 1 1 5C64C1A9
P 5950 3200
F 0 "R10" V 6150 3200 50  0000 C CNN
F 1 "1k" V 6050 3200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5880 3200 50  0001 C CNN
F 3 "~" H 5950 3200 50  0001 C CNN
	1    5950 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	6800 3200 6650 3200
Connection ~ 6650 3200
Wire Wire Line
	6250 3200 6100 3200
Wire Wire Line
	6400 3000 5600 3000
Connection ~ 5600 3000
Wire Wire Line
	5600 3000 5600 3200
Wire Wire Line
	5800 3200 5600 3200
Connection ~ 5600 3200
Text Notes 850  4350 0    50   ~ 0
Trans-impedance amplifier
Text Notes 3600 4350 0    50   ~ 0
Sallen-key 2nd order high pass filter OR\nNon-inverting amplifier (R6, R7, R9) OR\nVoltage buffer (short C5, C6, R9)
Wire Wire Line
	6250 2800 6250 3200
Wire Notes Line
	8650 1900 8650 4400
Wire Notes Line
	5700 1900 5700 4400
Wire Notes Line
	3450 1900 3450 4400
Wire Notes Line
	550  1900 550  4400
Text Notes 5800 4300 0    50   ~ 0
Regular comparator with hysteresis (do not place R10, C9 optional) OR\nEdge detector (R10 = non zero, do not place R11)
Wire Notes Line
	550  4400 8650 4400
Wire Notes Line
	550  1900 8650 1900
Wire Wire Line
	1900 1200 1800 1200
Wire Wire Line
	1800 1200 1800 1000
Connection ~ 1800 1000
Wire Wire Line
	1800 1000 1900 1000
$Comp
L Comparator:MCP6566 U3
U 1 1 5C694970
P 7100 3100
F 0 "U3" H 7441 3146 50  0000 L CNN
F 1 "MCP6566" H 7441 3055 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 7100 2700 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/DS20002143E.pdf" H 7100 3100 50  0001 C CNN
	1    7100 3100
	1    0    0    -1  
$EndComp
Connection ~ 7000 2800
$Comp
L Connector:TestPoint TP1
U 1 1 5C62F427
P 3200 3450
F 0 "TP1" H 3250 3750 50  0000 R CNN
F 1 "TestPoint" H 3350 3650 50  0000 R CNN
F 2 "TestPoint:TestPoint_THTPad_D1.0mm_Drill0.5mm" H 3400 3450 50  0001 C CNN
F 3 "~" H 3400 3450 50  0001 C CNN
	1    3200 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 3450 3200 3300
Connection ~ 3200 3300
Wire Wire Line
	3200 3300 3150 3300
$EndSCHEMATC
